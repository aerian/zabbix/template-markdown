# Template OS Linux template description



## Summary
* [items](#items)
* [triggers](#triggers)
* [discoveries](#discoveries)
  * [Discovery Network interface discovery ](#discovery_network_interface_discovery)
  * [Discovery Mounted filesystem discovery ](#discovery_mounted_filesystem_discovery)

<a name="items" />

## Items
| name | key | type |
| ------------- |------------- |------------- |
| Maximum number of opened files | kernel.maxfiles | Zabbix agent |
| Maximum number of processes | kernel.maxproc | Zabbix agent |
| Number of running processes | proc.num[,,run] | Zabbix agent |
| Number of processes | proc.num[] | Zabbix agent |
| Host boot time | system.boottime | Zabbix agent |
| Interrupts per second | system.cpu.intr | Zabbix agent |
| Processor load (1 min average per core) | system.cpu.load[percpu,avg1] | Zabbix agent |
| Processor load (5 min average per core) | system.cpu.load[percpu,avg5] | Zabbix agent |
| Processor load (15 min average per core) | system.cpu.load[percpu,avg15] | Zabbix agent |
| Context switches per second | system.cpu.switches | Zabbix agent |
| CPU $2 time | system.cpu.util[,idle] | Zabbix agent |
| CPU $2 time | system.cpu.util[,interrupt] | Zabbix agent |
| CPU $2 time | system.cpu.util[,iowait] | Zabbix agent |
| CPU $2 time | system.cpu.util[,nice] | Zabbix agent |
| CPU $2 time | system.cpu.util[,softirq] | Zabbix agent |
| CPU $2 time | system.cpu.util[,steal] | Zabbix agent |
| CPU $2 time | system.cpu.util[,system] | Zabbix agent |
| CPU $2 time | system.cpu.util[,user] | Zabbix agent |
| Host name | system.hostname | Zabbix agent |
| Host local time | system.localtime | Zabbix agent |
| Free swap space | system.swap.size[,free] | Zabbix agent |
| Free swap space in % | system.swap.size[,pfree] | Zabbix agent |
| Total swap space | system.swap.size[,total] | Zabbix agent |
| System information | system.uname | Zabbix agent |
| System uptime | system.uptime | Zabbix agent |
| Number of logged in users | system.users.num | Zabbix agent |
| Checksum of $1 | vfs.file.cksum[/etc/passwd] | Zabbix agent |
| Available memory | vm.memory.size[available] | Zabbix agent |
| Total memory | vm.memory.size[total] | Zabbix agent |


<a name="triggers" />

## Triggers
| name | expression | description | tags | url |
| ------------- |------------- |------------- |------------- |------------- |
| /etc/passwd has been changed on {HOST.NAME} | {Template_OS_Linux:vfs.file.cksum[/etc/passwd].diff(0)}>0 |  |  |  |
| Configured max number of opened files is too low on {HOST.NAME} | {Template_OS_Linux:kernel.maxfiles.last(0)}<1024 |  |  |  |
| Configured max number of processes is too low on {HOST.NAME} | {Template_OS_Linux:kernel.maxproc.last(0)}<256 |  |  |  |
| Disk I/O is overloaded on {HOST.NAME} | {Template_OS_Linux:system.cpu.util[,iowait].avg(5m)}>20 | OS spends significant time waiting for I/O (input/output) operations. It could be indicator of performance issues with storage system. |  |  |
| Host information was changed on {HOST.NAME} | {Template_OS_Linux:system.uname.diff(0)}>0 |  |  |  |
| Hostname was changed on {HOST.NAME} | {Template_OS_Linux:system.hostname.diff(0)}>0 |  |  |  |
| Lack of available memory on server {HOST.NAME} | {Template_OS_Linux:vm.memory.size[available].last(0)}<20M |  |  |  |
| Lack of free swap space on {HOST.NAME} | {Template_OS_Linux:system.swap.size[,pfree].last(0)}<50 | It probably means that the systems requires more physical memory. |  |  |
| Processor load is too high on {HOST.NAME} | {Template_OS_Linux:system.cpu.load[percpu,avg1].avg(5m)}>5 |  |  |  |
| Too many processes on {HOST.NAME} | {Template_OS_Linux:proc.num[].avg(5m)}>300 |  |  |  |
| Too many processes running on {HOST.NAME} | {Template_OS_Linux:proc.num[,,run].avg(5m)}>30 |  |  |  |
| {HOST.NAME} has just been restarted | {Template_OS_Linux:system.uptime.change(0)}<0 |  |  |  |


<a name="discoveries" />

## Discoveries
| name | key | description | type | lifetime | delay |
| ------------- |------------- |------------- |------------- |------------- |------------- |
| Network interface discovery | net.if.discovery | Discovery of network interfaces as defined in global regular expression "Network interfaces for discovery". | Zabbix agent | 30d | 1h |
| Mounted filesystem discovery | vfs.fs.discovery | Discovery of file systems of different types as defined in global regular expression "File systems for discovery". | Zabbix agent | 30d | 1h |


<a name="discovery_network_interface_discovery" />

## Discovery Network interface discovery

### Items

| name | key | type |
| ------------- |------------- |------------- |
| Incoming network traffic on $1 | net.if.in[{#IFNAME}] | Zabbix agent |
| Outgoing network traffic on $1 | net.if.out[{#IFNAME}] | Zabbix agent |


<a name="discovery_mounted_filesystem_discovery" />

## Discovery Mounted filesystem discovery

### Items

| name | key | type |
| ------------- |------------- |------------- |
| Free inodes on $1 (percentage) | vfs.fs.inode[{#FSNAME},pfree] | Zabbix agent |
| Free disk space on $1 | vfs.fs.size[{#FSNAME},free] | Zabbix agent |
| Free disk space on $1 (percentage) | vfs.fs.size[{#FSNAME},pfree] | Zabbix agent |
| Total disk space on $1 | vfs.fs.size[{#FSNAME},total] | Zabbix agent |
| Used disk space on $1 | vfs.fs.size[{#FSNAME},used] | Zabbix agent |


### Triggers

| name | expression | description | tags | url |
| ------------- |------------- |------------- |------------- |------------- |
| Free disk space is less than 20% on volume {#FSNAME} | {Template_OS_Linux:vfs.fs.size[{#FSNAME},pfree].last(0)}<20 |  |  |  |
| Free inodes is less than 20% on volume {#FSNAME} | {Template_OS_Linux:vfs.fs.inode[{#FSNAME},pfree].last(0)}<20 |  |  |  |

