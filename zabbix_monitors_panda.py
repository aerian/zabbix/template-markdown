# -*- coding: utf-8 -*-
"""Zabbix / Monitors Markdown

Parse the result from Zabbix JSON template and generate a
Markdown Document from it.

Usage available with -h|--help argument.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
import json
import argparse
import logging
import pandas as pd
from pathlib import Path


zabbix_items_types = {
    "0": "Zabbix agent",
    "1": "SNMPv1 agent",
    "2": "Zabbix trapper",
    "3": "simple check",
    "4": "SNMPv2 agent",
    "5": "Zabbix internal",
    "6": "SNMPv3 agent",
    "7": "Zabbix agent (active)",
    "8": "Zabbix aggregate",
    "9": "web item",
    "10": "external check",
    "11": "database monitor",
    "12": "IPMI agent",
    "13": "SSH agent",
    "14": "TELNET agent",
    "15": "calculated",
    "16": "JMX agent",
}
_MARKDOWN_CHARACTERS_TO_ESCAPE = set(r"!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~")
logger = logging.getLogger(__name__)


def markdown_table(rows=[]):
    """Generate a markdown table from given list.

    Args:
        rows (List): Data used to generate the table,
                     the first line is the header.

    Returns:
        result (String): Formatted string
    """
    result = "| " + " | ".join(rows[0].keys()) + " |\n| "
    for h in rows[0].keys():
        result += "------------- |"
    result += "\n"
    for r in rows:
        escaped = [s.translate(str.maketrans({"|": r"\|"})) for s in r.values()]
        result += "| " + " | ".join(escaped) + " |\n"
    return result


def parse_args():
    """ Parse the scripts args
    """
    parser = argparse.ArgumentParser(
        description="""Extract Data from Zabbix
             JSON to put them into a table"""
    )
    # Manage required args
    required = parser.add_argument_group("required arguments")
    # Manage optional args
    required.add_argument(
        "--file",
        "-f",
        dest="file",
        action="store",
        help="File to be used.",
        required=True,
    )
    required.add_argument(
        "--export-filename",
        "-e",
        dest="export_filename",
        action="store",
        help="Filename to be used for export of md & excel.",
        required=False,
    )
    required.add_argument(
        "--export-excel",
        "-x",
        dest="export_excel",
        action="store_true",
        help="Activate excel export.",
        required=False,
    )
    required.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        action="store_true",
        help="Activate debug.",
        required=False,
    )
    
    return parser.parse_args()

def unescape_excel(df) -> str:
    return df.apply(lambda x: x.str.replace('<br>', '')).apply(lambda x: x.str.replace(r"\\([!\"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~])", r'\1', regex=True))

def escaped_markdown(text: str) -> str:
    """Escape markdown special chars.

    Args:
        text (String): Data string to be escaped

    Returns:
        result (String): Formatted string
    """
    return "".join(
        f"\\{character}" if character in _MARKDOWN_CHARACTERS_TO_ESCAPE else character 
        for character in text
    )

def return_content_string(content):
    """Generate a string from nested struct if needed.

    Args:
        content (data): Data to be change to string format

    Returns:
        result (String): Formatted string
    """
    if isinstance(content, list) and not isinstance(content[0], dict):
        return escaped_markdown(",".join(content))
    elif isinstance(content, dict) or isinstance(content, list):
        return escaped_markdown(json.dumps(content))
    else:
        return escaped_markdown(str(content))


def generate_list(items=[], keys=["name", "key", "type"]):
    """Generate a list based on given data.

    Args:
        items (List): Data used to generate the dict,
        keys (List): List of the list key.

    Returns:
        result (List): Generated list.
    """
    table = []
    for i in items:
        try:
            row = {
                k: return_content_string(i[k])
                .replace("\n", "<br>")
                .replace("\r", "") if k in i else f"no {k}"
                for k in keys
            }
            table.append(row)
        except:
            logger.exception(f"Failed to process {keys} for item {i}")
    return table


def generate_markdown_result(template_name, template_description, data, df):
    """Generate a string from nested struct if needed.

    Args:
        template_name (String): Name of the template
        template_description (String): Description of the template
        data (Dict): Dictionnary content of template
        df (Dataframe): Panda daframe generated from dict

    Returns:
        result (String): Formatted text
    """
    # Generate full MD
    result = "# " + template_name + " template description\n"
    result += template_description + "\n"

    # Summary
    result += "## Summary\n"
    if len(data["items"]) > 0:
        result += "* [items](#items)\n"
    if len(data["macros"]) > 0:
        result += "* [macros](#macros)\n"
    if len(data["triggers"]) > 0:
        result += "* [triggers](#triggers)\n"
    if len(data["discovery_rules"]) > 0:
        result += "* [discoveries](#discoveries)\n"
        for d in data["discovery_rules_items"]:
            result += """  * [Discovery {} ](#discovery_{})\n""".format(
                d, d.lower().replace(" ", "_"))

    # Items
    if len(data["items"]) > 0:
        result += '\n<a name="items" />\n'
        result += "\n## Items\n"
        result += '\n'
        result += df["items"].to_markdown(index=False)
        result += '\n'

    # Macros
    if len(data["macros"]) > 0:
        result += '\n<a name="macros" />\n'
        result += "\n## Macros\n"
        result += '\n'
        result += df["macros"].to_markdown(index=False)
        result += '\n'

    # Triggers
    if len(data["triggers"]) > 0:
        result += '\n<a name="triggers" />\n'
        result += "\n## Triggers\n"
        result += '\n'
        result += df["triggers"].to_markdown(index=False)
        result += '\n'

    # Discovery
    if len(data["discovery_rules"]) > 0:
        result += '\n<a name="discoveries" />\n'
        result += "\n## Discoveries\n"
        result += '\n'
        result += df["discovery_rules"].to_markdown(index=False)
        result += '\n'

        # Discovery items
        for d in data["discovery_rules_items"]:
            result += '\n<a name="discovery_' + d.lower().replace(" ", "_") + '" />\n'
            result += "\n## Discovery " + d + "\n"
            result += "\n### Items" + "\n"
            result += '\n'
            result += df["discovery_rules_items"][d].to_markdown(index=False)
            result += '\n'
            # If there is triggers
            if len(data["discovery_rules_triggers"]) and d in data["discovery_rules_triggers"] and len(data["discovery_rules_triggers"][d]) > 0:
                result += "\n### Triggers" + "\n"
                result += '\n'
                result += df["discovery_rules_triggers"][d].to_markdown(index=False)
                result += '\n'
    return result


def main(args):
    """Main function of the script."""
    # Init part
    template_name = ""
    template_description = ""
    data = {}
    data["items"] = []
    data["triggers"] = []
    data["macros"] = []
    data["discovery_rules"] = []
    data["discovery_rules_items"] = {}
    data["discovery_rules_triggers"] = {}
    df = {}
    df["items"] = []
    df["triggers"] = []
    df["macros"] = []
    df["discovery_rules"] = []
    df["discovery_rules_items"] = {}
    df["discovery_rules_triggers"]= {}

    with open(args.file, "r") as fp:
        obj = json.load(fp)
        if len(obj["zabbix_export"]["templates"]) > 1:
            print("Will not process for more than one template in export")
        template = obj["zabbix_export"]["templates"][0]
        template_name = template["name"]
        template_description = template["description"]

        # Items part
        if "items" in template:
            data["items"] = generate_list(template["items"], keys=["name", "description", "delay", "key", "type"])

            for i in data["items"]:
                if i["type"] in zabbix_items_types:
                    i["type"] = zabbix_items_types[i["type"]]
            
            # Extract triggers from items
            for i in template["items"]:
                if "triggers" in i:
                    data["triggers"].extend(generate_list(i["triggers"],keys=["name", "priority", "description", "expression", "tags", "url"]))

            df["items"] = pd.DataFrame(data["items"])
        # Macros part
        if "macros" in template:
            if "description" in template["macros"][0].keys():
                macro_keys = ["macro", "value", "description"]
            else:
                macro_keys = ["macro", "value"]
            data["macros"] = generate_list(template["macros"], keys=macro_keys)
            df["macros"] = pd.DataFrame(data["macros"])
        # triggers part
        if "triggers" in obj["zabbix_export"]:
            data["triggers"].extend(generate_list(
                obj["zabbix_export"]["triggers"],
                keys=["name", "priority", "description", "expression", "tags", "url"],
            ))
            logger.debug(f"trigger list {data['triggers']}")
        df["triggers"] = pd.DataFrame(data["triggers"])

        if "discovery_rules" in template:
            data["discovery_rules"] = generate_list(
                template["discovery_rules"],
                keys=["name", "key", "description", "type", "lifetime", "delay"],
            )
            df["discovery_rules"] = pd.DataFrame(data["discovery_rules"])

            for i in data["discovery_rules"]:
                if i["type"] in zabbix_items_types:
                    i["type"] = zabbix_items_types[i["type"]]

            for d in template["discovery_rules"]:
                data["discovery_rules_triggers"][d["name"]] = []
                # discovery items part
                if "item_prototypes" in d:
                    items = generate_list(d["item_prototypes"], keys=["name", "description", "delay", "key", "type"])

                    for i in items:
                        if i["type"] in zabbix_items_types:
                            i["type"] = zabbix_items_types[i["type"]]

                    for t in [t["trigger_prototypes"] for t in d["item_prototypes"] if "trigger_prototypes" in t]:
                        # trigger item level
                        triggers = generate_list(
                            t,
                            keys=["name", "priority", "description", "expression", "tags", "url"],
                        )

                        data["discovery_rules_triggers"][d["name"]].extend(triggers)
                    data["discovery_rules_items"][d["name"]] = items

                # If trigger at template level
                if "trigger_prototypes" in d:
                # discovery triggers part
                    triggers = generate_list(
                        d["trigger_prototypes"],
                        keys=["name", "priority", "description", "expression", "tags", "url"],
                    )
                    data["discovery_rules_triggers"][d["name"]].extend(triggers)
                if d["name"] in data["discovery_rules_items"] and len(data["discovery_rules_items"][d["name"]]) > 0:
                    df["discovery_rules_items"][d["name"]] = pd.DataFrame(data["discovery_rules_items"][d["name"]])
                if d["name"] in data["discovery_rules_triggers"] and len(data["discovery_rules_triggers"][d["name"]]) > 0:
                    df["discovery_rules_triggers"][d["name"]] = pd.DataFrame(data["discovery_rules_triggers"][d["name"]])
            # discovery host part TODO

    # Generate markdown text
    result = generate_markdown_result(template_name=template_name, template_description=template_description, data=data, df=df)
    # Write or printout markdown text
    if args.export_filename:
        with open(str(Path(args.export_filename).stem)+".md", 'w') as f:
            f.write(result)
    else:
        print(result)

    # Generate excel file
    if args.export_excel:
       with pd.ExcelWriter(str(Path(args.export_filename).stem) + ".xlsx") as writer:
            unescape_excel(df["items"]).to_excel(writer, sheet_name='Items', index=False)
            unescape_excel(df["triggers"]).to_excel(writer, sheet_name='Triggers', index=False)
            unescape_excel(df["macros"]).to_excel(writer, sheet_name='Macros', index=False)
            unescape_excel(df["discovery_rules"]).to_excel(writer, sheet_name='Discovery rules', index=False)
            for d in data["discovery_rules_items"]:
                unescape_excel(df["discovery_rules_items"][d]).to_excel(writer, sheet_name='Items {}'.format(d), index=False)
                if len(data["discovery_rules_triggers"]) and d in data["discovery_rules_triggers"] and len(data["discovery_rules_triggers"][d]) > 0:
                    unescape_excel(df["discovery_rules_triggers"][d]).to_excel(writer, sheet_name='Triggers {}'.format(d), index=False)


if __name__ == "__main__":
    args = parse_args()
    if args.verbose:
        logging.basicConfig(encoding='utf-8', level=logging.DEBUG)
    main(args)
