# template-markdown

Proof of concept, Generation of Markdown from Zabbix JSON template.

## Requirements

Python 3.6

## Usage

```
usage: zabbix_monitors_markdown.py [-h] --file FILE

Extract Data from Zabbix JSON to put them into a table

optional arguments:
  -h, --help            show this help message and exit

required arguments:
  --file FILE, -f FILE  File to be used.
```

## Example of CI/CD

Below find a Gitlab CI example used to generate a Wiki page for a JSON template

```
.wiki: &wiki |
 export WIKI="https://${GITLAB_USER}:${GITLAB_TOKEN}@${GITLAB_HOST}/${CI_PROJECT_PATH}.wiki.git"
 if [ -f ${CI_PROJECT_DIR}/${CI_PROJECT_TITLE}.json ] ; then
   python /zabbix_monitors_markdown.py -f ${CI_PROJECT_DIR}/${CI_PROJECT_TITLE}.json > home.md
 else
   echo "Template ${CI_PROJECT_TITLE} not found in Zabbix" > home.md
 fi
 cat home.md
 DATE=`date '+%Y-%m-%d %H:%M'`
 printf "\n\n **Last updated on $DATE UTC +00:00**" >> home.md
 if [ $(git ls-remote ${WIKI}|wc -l) -eq 0 ] ; then
   echo "Wiki ${WIKI} does not exist, creating it"
 else
   echo "Wiki ${WIKI} exist, cloning it"
   git clone ${WIKI}
   cd ${CI_PROJECT_NAME}.wiki
   cp -f ../home.md home.md
   git add home.md
   if [ $(git status|grep -c "Changes to be committed") ] ; then
     git commit -m "update wiki"
     git push ${WIKI} HEAD:master
   fi
 fi
```

## Development requirements

* Have a working Python 3 environment:
```
virtualenv -p `which python3` venv
source venv/bin/activate
```
* Install the requirements `pip install -r requirements-dev.txt`
* Install the precomit hook `pre-commit install`

# Example

Please find below an example of generated markdown based on
[Template_OS_Linux JSON](./Template_OS_Linux.md)

